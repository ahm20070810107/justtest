# This file is a template, and might need editing before it works on your project.
FROM golang:1.14.7-alpine3.12
WORKDIR $GOPATH/src/github.com/mygo
#将服务器的go工程代码加入到docker容器中
ADD . $GOPATH/src/github.com/mygo
##go构建可执行文件
RUN go build .
##最终运行docker的命令
ENTRYPOINT  ["./mygo"]
